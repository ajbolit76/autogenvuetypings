using VueCodeGen.CodeGen;
using VueCodeGen.Implementations;

var builder = WebApplication.CreateBuilder(args);

var td = new TypeScriptTypeDefinition()
{
    Members = (new TypeScriptTypeMemberDeclarationBase[]
    {
        new TypeScriptTypeMemberDeclaration()
        {
            IsOptional = true,
            Name = "kek",
            Type = new TypeScriptBuiltInType("string")
        },
        new TypeScriptTypeMemberDeclaration()
        {
            IsOptional = false,
            Name = "test",
            Type = new TypeScriptFunctionType()
            {
                Arguments = { 
                    new TypeScriptArgumentDeclaration()
                    {
                        Name = "test",
                        Type = new TypeScriptBuiltInType("number")
                    }
                },
                Result = new TypeScriptBuiltInType("string")
            }
        }
    }).ToList()
};
var a = new TypeScriptTypeDeclaration()
{
    Definition = td,
    Name = "myType"
};
var b = new Dictionary<int, int>();
Console.WriteLine(a.GenerateCode(new DefaultCodeGenContext()));

// Add services to the container.
//
// builder.Services.AddControllers();
// // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
// builder.Services.AddEndpointsApiExplorer();
// builder.Services.AddSwaggerGen();
//
// var app = builder.Build();
//
// // Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
//     app.UseSwagger();
//     app.UseSwaggerUI();
// }
//
// app.UseHttpsRedirection();
//
// app.UseAuthorization();
//
// app.MapControllers();
//
// app.Run();