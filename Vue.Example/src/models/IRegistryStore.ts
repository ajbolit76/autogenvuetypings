import { IListDto } from './IListDto';

// TODO: Generic id in IListDto
export interface IRegistryChange<T extends IListDto<number>>{
    update: T[],
    delete: T[],
    create: T[],
}

export interface IRegistryStore<T extends IListDto<number>> {
    items: T[];
    count: number;
    page: number;
    itemsPerPage: number;
    changes: IRegistryChange<T>
    isLoading: boolean;
 //   get isDirty(): boolean;
}
