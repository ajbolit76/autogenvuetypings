﻿namespace VueCodeGen.Internals;

internal record ImportedSymbol(string SourceName, string LocalName, string Path);
