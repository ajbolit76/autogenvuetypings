﻿namespace VueCodeGen.Abstractions;

public interface ICodeGenContext
{
    public string Indentation { get; }
    public string NewLine { get; }
    string GetReferenceFromUnitToAnother(string currentUnit, string targetUnit);
}