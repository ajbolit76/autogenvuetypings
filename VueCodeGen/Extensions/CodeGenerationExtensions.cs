﻿using VueCodeGen.Abstractions;
using VueCodeGen.CodeGen;

namespace VueCodeGen.Extensions;

public static class CodeGenerationExtensions
{
    public static string GenerateCodeCommaSeparated(this IEnumerable<TypeScriptArgumentDeclaration> expressions, ICodeGenContext context)
    {
        return string.Join(", ", expressions.Select(x => x.GenerateCode(context)));
    }
    
    public static string GenerateCodeCommaSeparated(this IEnumerable<TypeScriptExpression> expressions, ICodeGenContext context)
    {
        return string.Join(", ", expressions.Select(x => x.GenerateCode(context)));
    }

    public static string GenerateCodeCommaSeparated(this IEnumerable<TypeScriptType> types, ICodeGenContext context)
    {
        return string.Join(", ", types.Select(x => x.GenerateCode(context)));
    }
}