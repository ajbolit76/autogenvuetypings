﻿using System.Text;

namespace VueCodeGen.Extensions;

public static class StringBuilderExtensions
{
    public static StringBuilder AppendWithIndent(this StringBuilder sb, string lines, string tab, string newLine)
    {
        var matchIndex = -1;
        sb.Append(tab);
        foreach (var ch in lines)
        {
            if (matchIndex+1 < newLine.Length && newLine[matchIndex+1] == ch)
            {
                matchIndex++;
            } else if (matchIndex + 1 == newLine.Length)
            {
                matchIndex = -1;
                sb.Append(tab);
            }
            
            sb.Append(ch);
        }

        return sb;
    }
}