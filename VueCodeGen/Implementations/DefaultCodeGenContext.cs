﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.Implementations;

public class DefaultCodeGenContext : ICodeGenContext
{
    public string Indentation { get; } = "    ";
    public string NewLine { get; } = "\n";
    public string GetReferenceFromUnitToAnother(string currentUnit, string targetUnit)
    {
        var path1 = new Uri(@"C:\a\a\a\a\a\a\a\a\" + currentUnit);
        var path2 = new Uri(@"C:\a\a\a\a\a\a\a\a\" + targetUnit);
        var diff = path1.MakeRelativeUri(path2);
        return "./" + diff.OriginalString;
    }
}