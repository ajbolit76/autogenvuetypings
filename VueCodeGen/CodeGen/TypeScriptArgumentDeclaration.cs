﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptArgumentDeclaration
{
    public string Name { get; set; }
    public bool Optional { get; set; }
    public TypeScriptType Type { get; set; }

    public string GenerateCode(ICodeGenContext context)
    {
        var optional = Optional ? "?" : "";
        return $"{Name}{optional}: {Type.GenerateCode(context)}";
    }
}