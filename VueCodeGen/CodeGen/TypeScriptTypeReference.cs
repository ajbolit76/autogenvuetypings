﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeReference : TypeScriptType
{
    public TypeScriptTypeReference(string name)
    {
        this.name = name;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return name;
    }

    private readonly string name;
}