﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptClassDeclaration : TypeScriptStatement
{
    public TypeScriptClassDeclaration(string name, TypeScriptClassDefinition defintion)
    {
        Name = name;
        Defintion = defintion;
    }

    public string Name { get; set; }
    public TypeScriptClassDefinition Defintion { get; set; }
    
    public override string GenerateCode(ICodeGenContext context)
    {
        return Defintion.GenerateBody(Name, context) + ";";
    }
}