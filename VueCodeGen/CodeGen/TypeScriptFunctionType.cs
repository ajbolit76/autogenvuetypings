﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptFunctionType: TypeScriptType
{
    public List<TypeScriptArgumentDeclaration> Arguments { get; } = new List<TypeScriptArgumentDeclaration>();

    public TypeScriptType Result { get; set; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"({Arguments.GenerateCodeCommaSeparated(context)}) => {Result.GenerateCode(context)}";
    }
}
