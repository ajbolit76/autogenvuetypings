﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptPromiseOfType : TypeScriptType
{
    public TypeScriptPromiseOfType(TypeScriptType targetType)
    {
        TargetType = targetType;
    }

    public TypeScriptType TargetType { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"Promise<{TargetType.GenerateCode(context)}>";
    }
}