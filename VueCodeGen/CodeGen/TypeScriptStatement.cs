﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public abstract class TypeScriptStatement
{
    public abstract string GenerateCode(ICodeGenContext context);
}