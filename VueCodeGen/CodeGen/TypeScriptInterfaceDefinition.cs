﻿using System.Text;
using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptInterfaceDefinition : TypeScriptType
{
    public List<TypeScriptInterfaceMember> Members { get; } = new List<TypeScriptInterfaceMember>();

    public override string GenerateCode(ICodeGenContext context)
    {
        var result = new StringBuilder();
        result.AppendFormat("{{").Append(context.NewLine);
        foreach (var member in Members)
        {
            result.AppendWithIndent(member.GenerateCode(context) + ";", context.Indentation, context.NewLine)
                .Append(context.NewLine);
        }

        result.Append("}");
        return result.ToString();
    }
}