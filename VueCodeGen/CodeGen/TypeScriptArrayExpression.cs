﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptArrayExpression : TypeScriptExpression
{
    public TypeScriptArrayExpression(params TypeScriptExpression[] items)
    {
        Items = new List<TypeScriptExpression>(items);
    }

    public List<TypeScriptExpression> Items { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"[{Items.GenerateCodeCommaSeparated(context)}]";
    }
}