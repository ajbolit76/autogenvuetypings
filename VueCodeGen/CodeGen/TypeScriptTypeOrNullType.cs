﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeOrNullType : TypeScriptUnionType, INullableWrapper
{
    public TypeScriptType InnerType { get; }

    public TypeScriptTypeOrNullType(TypeScriptType innerType) : base(new []{innerType, (TypeScriptBuiltInType)"null"})
    {
        InnerType = innerType ?? throw new ArgumentNullException(nameof(innerType), "Wrapped type is null");
    }
}