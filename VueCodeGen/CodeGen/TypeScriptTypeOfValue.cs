﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeOfValue : TypeScriptType
{
    public TypeScriptExpression TargetValue { get; set; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"typeof {TargetValue.GenerateCode(context)}";
    }
}