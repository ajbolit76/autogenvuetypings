﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public abstract class TypeScriptType
{
    public abstract string GenerateCode(ICodeGenContext context);
}