﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptExportTypeStatement : TypeScriptStatement
{
    public TypeScriptTypeDeclaration Declaration { get; init; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"export {Declaration.GenerateCode(context)}";
    }
}