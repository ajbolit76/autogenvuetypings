﻿namespace VueCodeGen.CodeGen;

public interface IWrapperType
{
    public TypeScriptType InnerType { get; }
}