﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public abstract class TypeScriptObjectLiteralInitializer
{
    public abstract string GenerateCode(ICodeGenContext context);
}