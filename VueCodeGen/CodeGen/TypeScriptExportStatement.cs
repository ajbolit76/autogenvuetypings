﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptExportStatement : TypeScriptStatement
{
    public override string GenerateCode(ICodeGenContext context)
    {
        return $"export {Declaration.GenerateCode(context)}";
    }

    public TypeScriptStatement Declaration { get; set; }
}