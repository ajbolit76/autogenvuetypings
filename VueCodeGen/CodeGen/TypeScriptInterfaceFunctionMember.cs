﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptInterfaceFunctionMember : TypeScriptInterfaceMember
{
    public TypeScriptInterfaceFunctionMember(string name, TypeScriptType result, params TypeScriptArgumentDeclaration[] arguments)
    {
        Name = name;
        Result = result;
        Arguments = new List<TypeScriptArgumentDeclaration>(arguments);
    }

    public string Name { get; }
    public TypeScriptType Result { get; }
    public List<TypeScriptArgumentDeclaration> Arguments { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{Name}({Arguments.GenerateCodeCommaSeparated(context)}): {Result.GenerateCode(context)}";
    }
}