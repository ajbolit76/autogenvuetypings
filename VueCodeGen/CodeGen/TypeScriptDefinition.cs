﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public abstract class TypeScriptDefinition
{
    public abstract string GenerateCode(string name, ICodeGenContext context);
}