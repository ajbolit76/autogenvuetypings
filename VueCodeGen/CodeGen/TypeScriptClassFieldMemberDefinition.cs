﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptClassFieldMemberDefinition : TypeScriptClassMemberDefinition
{
    public string Name { get; init; }

    public TypeScriptType Type { get; init; }
    
    public TypeScriptAccessModifier? AccessModifier { get; init; }
    
    public bool IsOptional { get; init; }
    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{AccessModifier?.GetCode() ?? ""} {Name}: {Type.GenerateCode(context)};";
    }
}