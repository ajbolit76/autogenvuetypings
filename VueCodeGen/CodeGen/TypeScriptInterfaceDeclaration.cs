﻿using System.Text;
using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptInterfaceDeclaration: TypeScriptTypeDeclaration
{
    public override string GenerateCode(ICodeGenContext context)
    {
        var result = new StringBuilder();
        result.AppendFormat("interface {0} ", Name);
        result.Append(Definition!.GenerateCode(context));
        return result.ToString();
    }
}