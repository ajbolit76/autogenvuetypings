﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptBuiltInType : TypeScriptType
{
    public string Name { get; }

    public TypeScriptBuiltInType(string name)
    {
        Name = name;
    }
    
    public override string GenerateCode(ICodeGenContext context)
    {
        return Name;
    }

    public static explicit operator TypeScriptBuiltInType(string name)
    {
        return new TypeScriptBuiltInType(name);
    }
}