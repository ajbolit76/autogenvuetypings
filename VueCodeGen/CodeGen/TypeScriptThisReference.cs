﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptThisReference : TypeScriptExpression
{
    public override string GenerateCode(ICodeGenContext context)
    {
        return "this";
    }
}