﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptArrayType : TypeScriptType
{
    public TypeScriptArrayType(TypeScriptType itemType)
    {
        ItemType = itemType;
    }

    private TypeScriptType ItemType { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        var innerTypeCode = ItemType.GenerateCode(context);
        if (!(ItemType is TypeScriptUnionType))
            return innerTypeCode + "[]";

        return $"Array<{innerTypeCode}>";
    }
}