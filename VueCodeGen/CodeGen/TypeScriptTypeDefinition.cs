﻿using System.Text;
using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeDefinition : TypeScriptType
{
    public List<TypeScriptTypeMemberDeclarationBase> Members { get; init; } =
        new List<TypeScriptTypeMemberDeclarationBase>();
    public override string GenerateCode(ICodeGenContext context)
    {
        var sb = new StringBuilder(1000);
        sb.Append('{').Append(context.NewLine);
        foreach (var member in Members)
        {
            sb.AppendWithIndent(member.GenerateCode(context), context.Indentation, context.NewLine).Append(context.NewLine);
        }
        sb.Append('}');
        return sb.ToString();
    }
}