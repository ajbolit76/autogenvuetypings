﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptGenericTypeReference : TypeScriptType
{
    public TypeScriptGenericTypeReference(TypeScriptTypeReference genericTypeReference, TypeScriptType[] genericArguments)
    {
        this.genericTypeReference = genericTypeReference;
        this.genericArguments = genericArguments;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{genericTypeReference.GenerateCode(context)}<{genericArguments.GenerateCodeCommaSeparated(context)}>";
    }

    private readonly TypeScriptTypeReference genericTypeReference;
    private readonly TypeScriptType[] genericArguments;
}