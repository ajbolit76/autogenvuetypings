﻿using System.Text;
using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptFunctionDefinition : TypeScriptDefinition
{
    public List<TypeScriptArgumentDeclaration> Arguments { get; } = new List<TypeScriptArgumentDeclaration>();

    public List<TypeScriptStatement> Body { get; } = new List<TypeScriptStatement>();

    public TypeScriptType Result { get; set; }
    public bool IsAsync { get; set; }
    public bool IsStatic { get; set; }
    public TypeScriptAccessModifier AccessModifier { get; set; } = TypeScriptAccessModifier.Public;

    public override string GenerateCode(string name, ICodeGenContext context)
    {
        var result = new StringBuilder(1000);
        result.Append($"{AccessModifier.GetCode()} ");
        if (IsStatic)
        {
            result.Append("static ");
        }

        if (IsAsync)
        {
            result.Append("async ");
        }

        result.Append(
            $"{name}({Arguments.GenerateCodeCommaSeparated(context)}): {Result.GenerateCode(context)} {{{context.NewLine}");
        foreach (var statement in Body)
        {
            result.AppendWithIndent(statement.GenerateCode(context), context.Indentation, context.NewLine)
                .Append(context.NewLine);
        }

        result.Append("}");
        return result.ToString();
    }
}