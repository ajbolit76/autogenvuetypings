﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen.ImportTypes;

internal class TypeScriptImportFromPathStatement : TypeScriptImportStatement
{
    public override string GenerateCode(ICodeGenContext context)
    {
        return $"import {{ {TypeName} }} from '{context.GetReferenceFromUnitToAnother(CurrentUnit.Path, PathToUnit)}';";
    }

    public string TypeName { get; set; }
    public string PathToUnit { get; set; }
    public TypeScriptUnit CurrentUnit { get; set; }
}