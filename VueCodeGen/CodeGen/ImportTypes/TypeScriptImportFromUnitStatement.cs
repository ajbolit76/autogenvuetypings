﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen.ImportTypes;

internal class TypeScriptImportFromUnitStatement : TypeScriptImportStatement
{
    public override string GenerateCode(ICodeGenContext context)
    {
        return $"import {{ {TypeName} }} from '{context.GetReferenceFromUnitToAnother(CurrentUnit.Path, TargetUnit.Path)}';";
    }

    public string TypeName { get; set; }
    public TypeScriptUnit TargetUnit { get; set; }
    public TypeScriptUnit CurrentUnit { get; set; }
}