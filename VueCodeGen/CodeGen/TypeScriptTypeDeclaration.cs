﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeDeclaration : TypeScriptType
{
    public string Name { get; init; }
    
    public TypeScriptType? Definition { get; init; }

    public string[] GenericTypeArguments { get; init; } = new string[]{};
    
    public override string GenerateCode(ICodeGenContext context)
    {
        var genericArgs = string.Empty;
        
        if(GenericTypeArguments.Length > 0)
            genericArgs = $"<{string.Join(", ", GenericTypeArguments)}>";
        
        return $"type {Name}{genericArgs} = {Definition?.GenerateCode(context)};";
    }
}