﻿using System.CodeDom.Compiler;
using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public abstract class TypeScriptTypeMemberDeclarationBase
{
    public abstract string GenerateCode(ICodeGenContext context);
}