﻿namespace VueCodeGen.CodeGen;

public enum AccessModifier
{
    Public,
    Protected,
    Private,
}

public class TypeScriptAccessModifier
{
    public static readonly TypeScriptAccessModifier Public =
        new TypeScriptAccessModifier(AccessModifier.Public, "public");

    public static readonly TypeScriptAccessModifier Protected =
        new TypeScriptAccessModifier(AccessModifier.Protected, "protected");
    
    public static readonly TypeScriptAccessModifier Private =
        new TypeScriptAccessModifier(AccessModifier.Private, "private");
    public AccessModifier AccessModifier { get; }

    private string _literal;

    private TypeScriptAccessModifier()
    {
        _literal = "";
    }
    
    private TypeScriptAccessModifier(AccessModifier modifier, string literal)
    {
        AccessModifier = modifier;
        _literal = literal;
    }
    
    public string GetCode()
    {
        return _literal;
    }
}