﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptInterfacePropertyMember : TypeScriptInterfaceMember
{
    public TypeScriptInterfacePropertyMember(string name, TypeScriptType result)
    {
        Name = name;
        Result = result;
    }

    public string Name { get; }
    public TypeScriptType Result { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{Name}: {Result.GenerateCode(context)}";
    }
}