﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptConstantExpression: TypeScriptExpression
{
    private readonly string _value;

    public TypeScriptConstantExpression(string value)
    {
        this._value = value;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return _value;
    }

}
