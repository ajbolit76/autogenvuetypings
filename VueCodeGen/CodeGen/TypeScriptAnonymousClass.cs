﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptAnonymousClass : TypeScriptExpression
{
    public string? Name { get; }
    
    public TypeScriptClassDefinition Definition { get; }

    public TypeScriptAnonymousClass(TypeScriptClassDefinition definition, string? name = null)
    {
        Name = name;
        Definition = definition;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"new {Definition.GenerateBody(Name, context)}();";
    }
}