﻿using System.Text;
using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptClassDefinition
{
    public List<TypeScriptClassMemberDefinition> Members { get; } = new List<TypeScriptClassMemberDefinition>();
    public TypeScriptType? BaseClass { get; set; }
    public TypeScriptType[]? ImplementedInterfaces { get; set; }

    public string GenerateBody(string? name, ICodeGenContext context)
    {
        var result = new StringBuilder(1000);
        result.Append("class ");

        if (name != null)
            result.Append(name).Append(' ');

        if (BaseClass != null)
            result.Append("extends ").Append(BaseClass.GenerateCode(context)).Append(' ');

        if (ImplementedInterfaces?.Any() == true)
            result.Append("implements ")
                .Append(string.Join(", ", ImplementedInterfaces.Select(x => x.GenerateCode(context)))).Append(' ');

        result
            .Append('{')
            .Append(context.NewLine);
        foreach (var member in Members)
        {
            result.AppendWithIndent(member.GenerateCode(context), context.Indentation, context.NewLine)
                .Append(context.NewLine)
                .Append(context.NewLine);
        }

        result.Append("}");
        return result.ToString();
    }
}