﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptArrowFunction : TypeScriptExpression
{
    public List<TypeScriptArgumentDeclaration> Arguments { get; } = new List<TypeScriptArgumentDeclaration>();

    public TypeScriptExpression Body { get; set; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"({Arguments.GenerateCodeCommaSeparated(context)}) => {Body.GenerateCode(context)}";
    }
}