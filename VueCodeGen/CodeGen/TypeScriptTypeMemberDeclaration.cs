﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeMemberDeclaration : TypeScriptTypeMemberDeclarationBase
{
    public TypeScriptType Type { get; init; }
    
    public string Name { get; init; }
    
    public bool IsOptional { get; init; }
    
    public override string GenerateCode(ICodeGenContext context)
    {
        return Name + (IsOptional ? "?: " : ": " ) + Type.GenerateCode(context)+";";
        
    }
}