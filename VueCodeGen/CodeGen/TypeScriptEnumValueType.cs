﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptEnumValueType : TypeScriptType
{
    public TypeScriptEnumValueType(TypeScriptType enumType, string value)
    {
        this._enumType = enumType;
        this._value = value;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{_enumType.GenerateCode(context)}.{_value}";
    }

    private readonly TypeScriptType _enumType;
    private readonly string _value;
}