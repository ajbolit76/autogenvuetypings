﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptReturnStatement : TypeScriptStatement
{
    public TypeScriptReturnStatement(TypeScriptExpression expression)
    {
        Expression = expression;
    }

    public TypeScriptExpression Expression { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"return {Expression.GenerateCode(context)};";
    }
}