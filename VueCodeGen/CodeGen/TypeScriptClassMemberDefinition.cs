﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptClassMemberDefinition
{
    public string Name { get; init; }

    public TypeScriptDefinition Definition { get; init; }
    
    public virtual string GenerateCode(ICodeGenContext context)
    {
        return Definition.GenerateCode(Name, context);
    }
}