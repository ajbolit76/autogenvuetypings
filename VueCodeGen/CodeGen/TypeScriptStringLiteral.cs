﻿using System.Text;
using System.Text.RegularExpressions;
using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public enum TypeScriptQuote
{
    DoubleQuote,
    SingleQuote,
    BackQuote
}

public class TypeScriptStringQuote
{
    private static readonly char DoubleQuote = '"';
    private static readonly char SingleQuote = '\'';
    private static readonly char BackQuote = '`';

    public char Quote { get; }
    
    public TypeScriptStringQuote(TypeScriptQuote quote)
    {
        Quote = quote switch
        {
            TypeScriptQuote.DoubleQuote => DoubleQuote,
            TypeScriptQuote.SingleQuote => SingleQuote,
            TypeScriptQuote.BackQuote => BackQuote,
            _ => throw new ArgumentOutOfRangeException(nameof(quote), quote, null)
        };
    }
}

public class TypeScriptStringLiteral : TypeScriptExpression
{
    
    public TypeScriptStringLiteral(string value, TypeScriptStringQuote? quote = null, bool interpolated = false)
    {
        Value = value;
        Quote = interpolated ? new TypeScriptStringQuote(TypeScriptQuote.BackQuote) 
            : quote ?? new TypeScriptStringQuote(TypeScriptQuote.DoubleQuote) ;
    }

    public string Value { get; }
    public TypeScriptStringQuote Quote { get; }
    public override string GenerateCode(ICodeGenContext context)
    {
        return FormQuotes(Value, Quote.Quote);
    }

    private string FormQuotes(string input, char quote)
    {
        var sb = new StringBuilder(input.Length + input.Count(x => x == quote) + 5);
            var escapeStarted = false;

        sb.Append(quote);
        for (var i = 0; i < input.Length; i++)
        {
            var ch = input[i];
            
            if (ch == '\\' && !escapeStarted)
                escapeStarted = true;
            else if (ch == quote && !escapeStarted)
                sb.Append('\\');
            else if (escapeStarted)
                escapeStarted = false;
            
            sb.Append(ch);
        }
        sb.Append(quote);
        return sb.ToString();
    }
}