﻿using VueCodeGen.Abstractions;
using VueCodeGen.Extensions;

namespace VueCodeGen.CodeGen;

public class TypeScriptConstructorCallExpression: TypeScriptExpression
{
    public TypeScriptConstructorCallExpression(TypeScriptExpression className, params TypeScriptExpression[] arguments)
    {
        ClassName = className;
        Arguments = new List<TypeScriptExpression>(arguments);
    }

    public TypeScriptExpression ClassName { get; }
    public List<TypeScriptExpression> Arguments { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"new {ClassName.GenerateCode(context)}({Arguments.GenerateCodeCommaSeparated(context)})";
    }
}