﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptObjectLiteralSpread : TypeScriptObjectLiteralInitializer
{
    public TypeScriptObjectLiteralSpread(TypeScriptVariableReference expression)
    {
        Expression = expression;
    }

    public TypeScriptVariableReference Expression { get; }
    
    public override string GenerateCode(ICodeGenContext context)
    {
        return $"...{Expression.GenerateCode(context)}";
    }
}