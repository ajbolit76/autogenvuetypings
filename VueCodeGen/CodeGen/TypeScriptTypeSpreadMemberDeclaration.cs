﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTypeSpreadMemberDeclaration : TypeScriptTypeMemberDeclarationBase
{
    public TypeScriptTypeSpreadMemberDeclaration(TypeScriptType type)
    {
        Type = type;
    }

    public TypeScriptType Type { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"...{Type.GenerateCode(context)};";
    }
}