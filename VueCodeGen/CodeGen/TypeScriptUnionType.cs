﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptUnionType : TypeScriptType
{
    private readonly TypeScriptType[] _types;

    public TypeScriptUnionType(TypeScriptType[] types)
    {
        _types = types;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        var joinedString = string.Join(" | ", _types.Select(x => x.GenerateCode(context)));
        return joinedString;
    }
}