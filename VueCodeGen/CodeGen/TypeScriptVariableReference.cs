﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptVariableReference : TypeScriptExpression
{
    public string Name { get; }
    
    public TypeScriptVariableReference(string name)
    {
        Name = name;
    }

    public override string GenerateCode(ICodeGenContext context)
    {
        return Name;
    }

}