﻿using VueCodeGen.Abstractions;

namespace VueCodeGen.CodeGen;

public class TypeScriptTernaryIfExpression : TypeScriptExpression
{
    public TypeScriptTernaryIfExpression(TypeScriptExpression condition, TypeScriptExpression trueBranch, TypeScriptExpression falseBranch)
    {
        Condition = condition;
        TrueBranch = trueBranch;
        FalseBranch = falseBranch;
    }

    public TypeScriptExpression Condition { get; }
    public TypeScriptExpression TrueBranch { get; }
    public TypeScriptExpression FalseBranch { get; }

    public override string GenerateCode(ICodeGenContext context)
    {
        return $"{Condition.GenerateCode(context)} ? {TrueBranch.GenerateCode(context)} : {FalseBranch.GenerateCode(context)}";
    }
}