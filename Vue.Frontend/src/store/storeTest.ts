import FoodApi from '@/api/FakeFoodApi';
import { IFoodListDto } from '@/models/FoodListDto';
import { IRegistryStore, IRegistryStoreData } from '@/models/IRegistryStore';
import { watchArrayElements } from '@/utils/proxyHandlers';
import { defineStore } from 'pinia';
import { reactive, readonly, watch } from 'vue';

let count = 0;
const createRegistryStore = (id = count += 1) => defineStore(`registry_${id}`, (): IRegistryStore<IFoodListDto> => {
  const api = new FoodApi();

  const data: IRegistryStoreData<IFoodListDto> = reactive({
    items: [],
    changes: { create: [], update: [], delete: [] },
    count: 0,
    isDirty: false,
    itemsPerPage: 10,
    page: 0,
    isLoading: false,
  });

  async function fetchAsync() {
    data.isLoading = true;
    data.items = watchArrayElements(await api.ListFood(), (op, newV, old, key) => console.log(`intercepted ${op} - ${String(key)} set new: ${newV} old: ${old}`));
    data.isLoading = false;
  }

  return { data, fetchAsync };
});

export default createRegistryStore;
