/* eslint-disable no-use-before-define */
import { Component } from 'vue';
import { IFoodListDto } from '../FoodListDto';
import { IListDto } from '../IListDto';
import { IRegistryStore } from '../IRegistryStore';

export interface IColumnProperties<TDto, K extends keyof TDto = keyof TDto> {
  id?: string;
  field: K;
  displayName: string;

  transform?: (from: TDto[K]) => string;

  hidden?: boolean;
  component?: Component<CellProps<TDto>>;
}

const test: IRegistryStore<IFoodListDto> = {
  data: {
    items: [],
    changes: { create: [], update: [], delete: [] },
    count: 0,
    itemsPerPage: 10,
    page: 0,
    isLoading: false,
  },
  fetchAsync: () => new Promise((resolve) => { resolve(); }),
};

type BaseCellProps<TDto> = {
  columnDefinition: IColumnProperties<TDto>;
  row: TDto;
}
// eslint-disable-next-line @typescript-eslint/ban-types
export type CellProps<TDto> =
  TDto extends IListDto<unknown> ?
    BaseCellProps<TDto> & { store?: IRegistryStore<TDto> } : BaseCellProps<TDto>;

export interface IRegistryParams<T extends IListDto<unknown>> {
  store: IRegistryStore<T>,
  columns: IColumnProperties<T>[]
}
