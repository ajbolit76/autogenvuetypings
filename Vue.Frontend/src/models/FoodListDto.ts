import { IDto } from './IDto';
import { IListDto } from './IListDto';

export interface IFoodListDto extends IListDto<number> {
  name: string;
  price: number;
  description: string;
}

export interface IFoodDto extends IDto {
  name: string;
  price: number;
  description: string;
}
