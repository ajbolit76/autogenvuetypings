import { IListDto } from './IListDto';

// TODO: Generic id in IListDto
export interface IRegistryChange<T extends IListDto<unknown>>{
    update: T[],
    delete: T[],
    create: T[],
}

export interface IRegistryStoreData<T extends IListDto<unknown>> {
    items: T[];
    count: number;
    page: number;
    itemsPerPage: number;
    changes: IRegistryChange<T>
    isLoading: boolean;
 //   get isDirty(): boolean;
}

export interface IRegistryStore<T extends IListDto<unknown>>{
    data: IRegistryStoreData<T>,
    fetchAsync: () => Promise<void>,
}
