/* eslint-disable no-use-before-define */
import {
  hasChanged, hasOwn, isArray, isIntegerKey, isObject, isSymbol, makeMap, toRawType,
} from './utils';

export enum ArrayOpType {
  Add,
  Set
}

export type ArrayChangeHandler<T> = (
  type: ArrayOpType,
  newValue: T,
  oldValue: T,
  key: symbol | string
) => void;
export type ArrayElementChange = () => void;

const builtInSymbols = new Set(
  Object.getOwnPropertyNames(Symbol)
    .map((key) => (Symbol as any)[key])
    .filter(isSymbol),
);
const isNonTrackableKeys = makeMap('__proto__,__v_isRef,__isVue');

export function createElementChangeHandler(handle: ArrayElementChange, shallow = false)
: ProxyHandler<object> {
  return {
    get: (target, key, receiver) => {
      if (key === WatchFlags.RAW
        && receiver === watchedMap.get((target as Target)[WatchFlags.VUE_RAW] || target)) {
        return target;
      }

      if (key === WatchFlags.IS_SHALLOW) {
        return shallow;
      }
      const res = Reflect.get(target, key, receiver);

      if (isSymbol(key) ? builtInSymbols.has(key) : isNonTrackableKeys(key)) {
        return res;
      }

      if (shallow) {
        return res;
      }

      if (isObject(res)) {
        return watchElement(res, createElementChangeHandler(handle));
      }

      return res;
    },
    set: (
      target,
      key,
      newValue,
      receiver,
    ) => {
      let oldValue = (target as any)[key];
      let value = newValue;

      if (!shallow) {
        if (!isShallow(value)) {
          value = toRaw(value);
          oldValue = toRaw(oldValue);
        }
      }

      const result = Reflect.set(target, key, value, receiver);

      handle();

      return result;
    },
  };
}

export function createArrayProxyHandler<T>(handler: ArrayChangeHandler<T>, shallow = false)
: ProxyHandler<Array<T>> {
  return {
    get: (target, key, receiver) => {
      if (key === WatchFlags.RAW
      && receiver === watchedMap.get((target as Target)[WatchFlags.VUE_RAW] || target)) {
        return target;
      }

      if (key === WatchFlags.IS_SHALLOW) {
        return shallow;
      }

      const raw = toRaw(target);

      const res = Reflect.get(target, key, receiver);

      if (isSymbol(key) ? builtInSymbols.has(key) : isNonTrackableKeys(key)) {
        return res;
      }

      if (isObject(res)) {
        // eslint-disable-next-line max-len
        return watchElement(res, createElementChangeHandler(() => handler(ArrayOpType.Set, raw[Number(key)], res, key), shallow));
      }

      return res;
    },
    set: (
      target,
      key,
      value,
      receiver,
    ) => {
      const oldValue = (target as any)[key];

      const hadKey = isIntegerKey(key)
        ? Number(key) < target.length
        : hasOwn(target, key);

      const result = Reflect.set(target, key, value, receiver);

      if (!hadKey) {
        handler(ArrayOpType.Add, value, oldValue, key);
      } else if (hasChanged(value, oldValue)) {
        handler(ArrayOpType.Set, value, oldValue, key);
      }

      return true;
    },
  };
}

export enum WatchFlags {
  VUE_RAW = '__v_raw',
  RAW = '__cr_raw',
  SKIP = '__cr_skip',
  IS_SHALLOW = '__cr_shallow',
}

export interface Target {
  [WatchFlags.SKIP]?: boolean
  [WatchFlags.IS_SHALLOW]?: boolean
  [WatchFlags.RAW]?: any
  [WatchFlags.VUE_RAW]?: any
}

export const watchedMap = new WeakMap<Target, any>();

function isAllowedType(rawType: string) {
  switch (rawType) {
    case 'Object':
    case 'Array':
      return true;
    default:
      return false;
  }
}

function isTargetValid(value: Target) {
  return value[WatchFlags.SKIP] || !Object.isExtensible(value)
    ? false
    : isAllowedType(toRawType(value));
}

export function watchElement(
  target: Target,
  handler: ProxyHandler<any>,
) {
  if (!isObject(target)) { return target; }

  if (target[WatchFlags.RAW]) { return target; }

  if (!isTargetValid(target)) { return target; }

  const existingProxy = watchedMap.get(target);
  if (existingProxy) {
    return existingProxy;
  }

  const proxy = new Proxy(target, handler);
  watchedMap.set(target, proxy);
  return proxy;
}

export function watchArrayElements<T>(
  target: Array<T>,
  onElementChange: ArrayChangeHandler<T>,
): Array<T> {
  if ((target as Target)[WatchFlags.RAW]) { return target; }

  const existingProxy = watchedMap.get(target as Target);
  if (existingProxy) {
    return existingProxy;
  }

  const proxy = new Proxy(target, createArrayProxyHandler(onElementChange));
  watchedMap.set(target as Target, proxy);
  return proxy;
}

export function isShallow(value: unknown): boolean {
  return !!(value && (value as Target)[WatchFlags.IS_SHALLOW]);
}

export function toRaw<T>(observed: T): T {
  const raw = observed && (observed as Target)[WatchFlags.RAW];
  return raw ? toRaw(raw) : observed;
}
