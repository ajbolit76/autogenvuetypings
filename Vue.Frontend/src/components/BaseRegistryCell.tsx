import { CellProps } from '@/models/components/RegistryParameters';
import { IListDto } from '@/models/IListDto';
import { FunctionalComponent } from 'vue';

export default function CreateRegistryCell<TDto extends IListDto<unknown>>()
: FunctionalComponent<CellProps<TDto>> {
  return (props, { slots, emit, attrs }) => {
    console.log(props.columnDefinition);
    return (
      <span>
        {props.row[props.columnDefinition.field]}
      </span>
    );
  };
}
