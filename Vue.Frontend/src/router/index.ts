import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import MainLayout from '@/views/MainLayout.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: MainLayout,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
