import { IFoodListDto } from '@/models/FoodListDto';
import { sleep } from '@/utils/utils';

export default class FoodApi {
  private _foodListDto: IFoodListDto[] = [{
    id: 0,
    name: 'Apple',
    price: 123,
    description: 'Apple grows on trees',
  }];

  public async GetFood(id: number): Promise<IFoodListDto | null> {
    await sleep(1000);
    return this._foodListDto.find((x) => x.id === id) ?? null;
  }

  public async ListFood(): Promise<IFoodListDto[]> {
    await sleep(1000);
    return this._foodListDto;
  }
}
