﻿using System.Collections.Generic;
using System.Linq;
using VueCodeGen.Abstractions;
using VueCodeGen.CodeGen;
using VueCodeGen.Implementations;
using Xunit;

namespace VueCodeGen.Tests.CodeGen;

public class TypeScriptStringLiteral_Test
{
    private ICodeGenContext _context = new DefaultCodeGenContext();

    public record TypeScriptTypeMemberDeclarationTestCase(string Value, TypeScriptQuote Quote, bool Interpolate, string Expectation);
    
    public static IEnumerable<object[]> Should_generate_correct_string_Data =>
        new List<TypeScriptTypeMemberDeclarationTestCase>
        {
            new ("Hello World", TypeScriptQuote.BackQuote, false, "`Hello World`"),
            new ("Hello ${World}", TypeScriptQuote.BackQuote, true, "`Hello ${World}`"),
            new ("Hello `mask` ${World}", TypeScriptQuote.BackQuote, true, "`Hello \\`mask\\` ${World}`"),
            new ("\"Sherlock Holmes\"", TypeScriptQuote.DoubleQuote, false, "\"\\\"Sherlock Holmes\\\"\""),
            new ("King\\'s crown", TypeScriptQuote.SingleQuote, false, "'King\\'s crown'"),
            new ("King's crown", TypeScriptQuote.SingleQuote, false, "'King\\'s crown'"),
            new ("King\\\\'s crown", TypeScriptQuote.SingleQuote, false, "'King\\\\\\'s crown'")
        }.Select(x=>new object[]{(object)x});
    
    [Theory]
    [MemberData(nameof(Should_generate_correct_string_Data))]
    public void Should_generate_correct_string_and_escape(TypeScriptTypeMemberDeclarationTestCase testCase)
    {
        var testObject = new TypeScriptStringLiteral(testCase.Value, new TypeScriptStringQuote(testCase.Quote), testCase.Interpolate);
        Assert.Equal(testObject.GenerateCode(_context), testCase.Expectation);
    }
}