﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using VueCodeGen.Abstractions;
using VueCodeGen.CodeGen;
using VueCodeGen.Implementations;
using Xunit;

namespace VueCodeGen.Tests.CodeGen;

public class TypeScriptTypeMemberDeclaration_Test
{
    private ICodeGenContext _context = new DefaultCodeGenContext();

    public record TypeScriptTypeMemberDeclarationTestCase(string Name, TypeScriptType Type, bool IsOptional,
        string Expectation);

    public static IEnumerable<object[]> Should_generate_correct_typedef_Data =>
        new List<TypeScriptTypeMemberDeclarationTestCase>
        {
            new("test", (TypeScriptBuiltInType)"bool", true, "test?: bool;"),
            new("test", (TypeScriptBuiltInType)"bool", false, "test: bool;")
        }.Select(x => new object[] { (object)x });

    [Theory]
    [MemberData(nameof(Should_generate_correct_typedef_Data))]
    public void Should_generate_correct_typedef(TypeScriptTypeMemberDeclarationTestCase testCase)
    {
        var testObject = new TypeScriptTypeMemberDeclaration()
        {
            Name = testCase.Name,
            Type = testCase.Type,
            IsOptional = testCase.IsOptional,
        };
        Assert.Equal(testObject.GenerateCode(_context), testCase.Expectation);
    }
}